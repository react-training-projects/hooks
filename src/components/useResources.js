import { useState, useEffect } from 'react';
import axios from 'axios';

export default resource => {
  const [resources, setResources] = useState([]);

  const fetchResource = async resource => {
    const url = `https://jsonplaceholder.typicode.com/${resource}`;
    const response = await axios.get(url);

    setResources(response.data);
  };

  useEffect(() => {
    fetchResource(resource);
  }, [resource]);

  return resources;
};
